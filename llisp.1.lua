--[[
-- <<license>>
--]]

llisp = { __version = 0.20 }

llisp.load = loadstring or load
llisp.unpack = table.unpack or unpack
llisp.global_env = {}

function llisp:split(s, sep)
  -- Splits a string into a table
  -- sep is required to not be a string that lua uses in pattern matches.
  -- There are workarounds for this, but it isn't needed at this very
  -- moment, and if I remember correctly the workaround looked kludgey.
  local t = {}
  for v in s:gmatch("([^"..sep.."]+)" .. "["..sep.."]*") do
    table.insert(t, v)
  end
  return t
end

function llisp:tostring(v, printkeys, noquotes)
  -- This is a drop-in replacement to tostring()
  -- The only difference is that it goes through tables and prints out
  -- the contents, instead of just giving a memory reference
  if type(v) == "string" then
    if not noquotes then return "\""..v.."\""
    else return v end
  elseif type(v) == "nil" then return "nil"
  elseif type(v) == "boolean" then
    if v then return "true" else return "false" end
  elseif type(v) == "table" then
    local t = {}
    for k, e in pairs(v) do
      if printkeys then table.insert(t, k .. "=") end
      table.insert(t, self:tostring(e, printkeys))
    end
    return "{" .. table.concat(t, ", ") .. "}"
  else
    return tostring(v)
  end
end

function llisp:debug_print(s, v)
  if _DEBUG then
    if v then
      io.write(s)
      print(self:_tostring(v, true))
    else
      print(llisp._tostring(s, true))
    end
  end
end

function llisp:table_copy(t)
  -- Returns a copy of t
  return {self.unpack(t)}
end

function llisp:table_append(t0, t1)
  -- If we end up doing this a lot it may be quicker to just inline it
  local t = self:table_copy(t0)
  for _,v in pairs(t1) do
    table.insert(t, v)
  end
  return t
end

function llisp:table_subset(list, n)
  -- Returns an identical list with the first n elements removed,
  -- by default n is 1
  local i
  local t = self.table_copy(list)
  if not n then n = 1 end
  if next(list) == nil then return nil end
  for i = 1, n do
    table.remove(t, 1)
  end
  return t
end

function llisp.stringp(c) return c == "\"" end
function llisp.spacep(c) return c:match("%s") end
function llisp.reservp(c) return c:match("[,'()]+") end

function llisp:gettoken(str, t, i)
  local c = str:sub(i, i)
  if c == ";" then
    return nil
  elseif self.stringp(c) then
    local s = ""
    repeat
      s = s .. c
      i = i + 1
      c = str:sub(i, i)
    until self.stringp(c)
    i = i + 1 -- Because a while would do this anyway
    table.insert(t, s .. "\"")
  elseif self.spacep(c) then
    repeat
      i = i + 1
      c = str:sub(i, i)
    until not self.spacep(c)
  elseif self.reservp(c) then
    table.insert(t, c)
    i = i + 1 -- Else we get an inf loop
  else -- We've got ourselves a symbol! Look at the lil' blighter!
    local s = ""
    while i <= #str and
          (not (self.stringp(c) or
                self.spacep(c)  or
                self.reservp(c))) do
      s = s .. c
      i = i + 1
      c = str:sub(i, i)
    end
    table.insert(t, s)
  end
  return i
end

function llisp:parse(t)
  -- Recursively construct a parsed table of t
  -- Throwing away input as we go
  if next(t) == nil then return nil, -1 end
  local v = table.remove(t, 1)
  if v == "(" then
    local nt = {}
    while t[1] ~= ")" do
      table.insert(nt, self:parse(t))
    end
    table.remove(t, 1)
    return {typ="list", val=nt}
  elseif tonumber(v) then
    return {typ="number", val=tonumber(v)}
  elseif v == "'" then
    -- We handle this in eval()
    return {typ="list", val={
            {typ="symbol", val="quote"},
            self:parse(t)}}
  elseif v == "," then
    -- We handle this inside handling in eval()
    return {typ="list", val={
            {typ="symbol", val="comma"},
            self:parse(t)}}
  elseif self.stringp(v:sub(1, 1)) then
    return {typ="string", val=v:sub(2, #v-1)}
  else
    return {typ="symbol", val=v}
  end
end

function llisp.iseof(s) return s end
function llisp.bracketmatchc(t)
  local c = 0
  for _, v in pairs(t) do
    if v == "(" then c = c + 1
    elseif v == ")" then c = c - 1 end
  end
  return c
end
function llisp:tokenize(s, h)
  local t = {} local i = 1
  while not (i > #s) do
    i = self:gettoken(s, t, i)
    -- We hit a comment, so grab another line if possible
    if not i and h then s = h:read("*l") i = 1 end
  end
  return t
end

function llisp:read_file(infile)
  local file, input, t
  local tokens = {}
  file = io.open(infile)
  if not file then return nil, "Failure: file does not exist!" end

  while true do
    input = file:read("*l")
    if not input then break end --hit eof

    t = self:tokenize(input, file)
    while self.bracketmatchc(t) > 0 do
      input = file:read("*l")
      if not input then break end -- eof!
      t = self:table_append(t, self:tokenize(input, file))
    end

    -- If there are /still/ unmatched brackets, then fail!
    if self.bracketmatchc(t) > 0 then
      return nil, "Failure: Brackets unmatched!"
    end
    -- Make sure we've not got an empty table.
    if next(t) ~= nil then table.insert(tokens, self:parse(t)) end
  end
  file:close()
  return tokens
end

function llisp:read(infile)
  local file, input, t
  -- Figure out where the input is coming from and grab the input
  if infile then file = io.open(infile) else file = io.stdin end
  if not file then return nil, "Failure: file does not exist!" end
  input = file:read("*l")

  t = self:tokenize(input, file)
  -- To make sure it's valid input we grab more if '(' is unmatched
  if file then -- If there's no file then we can't grab more :(
    while self.bracketmatchc(t) > 0 and self.iseof(input) do
      io.write("     > ")
      input = file:read("*l")
      t = self:table_append(t, self:tokenize(input, file))
    end
    file:close()
  end

  -- If there are /still/ unmatched brackets, then fail!
  if self.bracketmatchc(t) > 0 then
    return nil, "Failure: Brackets unmatched!"
  end
  return self:parse(t)
end

function llisp:pretty_print(v)
  if type(v) ~= "table" then print(self:tostring(v)) error("Uh Oh", 1) end
  if v.typ == "string" then
    return "\"" .. v.val .. "\""
  elseif v.typ == "number" then
    return tostring(v.val)
  elseif v.typ == "symbol" then
    return v.val
  elseif v.typ == "function" then
    return "<f:" .. tostring(v.val):sub(11) .. ">"
  elseif v.typ == "macro" then
    return "<m:" .. tostring(v.val):sub(11) .. ">"
  elseif v.typ == "list" then
    local t = {}
    for _,e in pairs(v.val) do
      table.insert(t, llisp:pretty_print(e))
    end
    return "(" .. table.concat(t, " ") .. ")"
  else
    -- Again, I have no idea what this value could be, but just in case
    print(llisp:tostring(v))
    error([[Found unexpected input to pretty_print!
            Please ask the maintainer to fix this asap!]], 1)
  end
end

function llisp:val_fromlua(v)
  -- Convert lua values into a format compatible with llisp
  -- Prototype is: {typ="<typename>", val=<value>}
  if type(v) == "table" and not v.typ then -- Make sure it's not llisp
    local t = {}
    for _,e in pairs(v) do
      table.insert(t, self:val_fromlua(e))
    end
    return {typ="list", val=t}
  elseif type(v) == "string" then
    return {typ="string", val=v}
  elseif type(v) == "number" then
    return {typ="number", val=v}
  elseif type(v) == "boolean" or type(v) == "nil" then
    if v then return {typ="symbol", val="t"}
    else      return {typ="symbol", val="nil"} end
  elseif type(v) == "function" then
    return {typ="function", val=v}
  else
    -- -- I have no idea what this could be, but just in case :0
    -- print("DEBUG: " .. v)
    -- error([[Got unexpected lua value!
    --       Please ask the maintainer to fix this asap!]], 2)
    return v
  end
end

function llisp:val_tolua(v, env)
  -- Convert llisp values into a format compatible with lua
  if type(v) == "table" then -- Make sure it's llisp
    if v.typ == "string" or v.typ == "number" or
       v.typ == "function" or v.typ == "macro" then
      return v.val
    elseif v.typ == "symbol" then
      return self:val_tolua(self:eval(v, env))
    elseif v.typ == "list" then
      local t = {}
      for _,e in pairs(v.val) do
        table.insert(t, self:val_tolua(e, env))
      end
      return t
    end
  else
    return v
  end
end

-- Entries in environments look like: {mutp=<bool>, val=<llisp_val>}
-- So to avoid doing inline jiggery-pokery that'd obfuscate things a bit
-- I'm providing these:
function llisp:val_get(sym, env)
  if not env then env = self.global_env end
  if not env[sym] then return nil end
  return env[sym].val
end
-- And this:
function llisp:val_set(sym, v, mutablep, env)
  if not env then env = self.global_env end
  -- If we get input that can't be accepted, try to convert it.
  if type(v) ~= "table" then v = self:val_fromlua(v) end
  if type(v) == "table" and not v.typ then v = self:val_fromlua(v) end

  if env[sym] and env[sym].mutp then return nil end
  -- ^ Here we rejoice because callers are responsible for everything
  -- [In evil voice] Evvvvryyythiiiinnngggg! *cackle*
  env[sym] = {mutp=mutablep, val=v}
  return true
end

function llisp:run_function(list, env)
  if list.typ ~= "list" then return nil, "Expected list, got " .. list.typ end
  local f = self:eval(list.val[1], env)
  local args = self:table_copy(list.val)
  table.remove(args, 1) -- Remove the function off the front
  for k, e in pairs(args) do
    args[k] = self:eval(e, env)
  end
  if type(f) == "table" and f.typ ~= "function" then
    return nil, "First arg of list: Function expected, got " .. f.typ
  end
  -- To avoid future trouble, I'm going to convert the return vals to llisp
  return self:val_fromlua(f.val(table.unpack(args)))
end

function llisp:run_macro(list, env)
  -- Grab the macro, separate the args from the macro, execute it.
  if list.typ ~= "list" then return nil, "Expected list, got " .. list.typ end
  local m = self:eval(list.val[1], env)
  local args = self:table_copy(list.val)
  table.remove(args, 1)
  if type(m) == "table" and m.typ ~= "macro" then
    return nil, "First arg of list: Macro expected, got " .. f.typ
  end
  -- To avoid future trouble, I'm going to convert the return vals to llisp
  return self:val_fromlua(m.val(table.unpack(args)))
end

function llisp:expand_macro(v, env)
  -- We MIIIGHT want to scan the list and run it recursively on that :1
  -- ... Miiiiiight
  if v and v.typ and v.typ == "list" then
    print(llisp:tostring(v.val[1]))
    local tmp = self:val_get(v.val[1].val, env)
    if tmp and tmp.typ and tmp.typ == "macro" then
      return self:run_macro(v, env)
    end
  end
  return v
end

function llisp:handle_comma(list, env)
  -- Standard procedure, scan through the list and replace the bits
  -- matching (quote ...)
  print(self:tostring(list))
  if list.typ == "list" then
    for k, e in pairs(list.val) do
      if e.typ == "list" and e.val[1] then
        local f = e.val[1]
        local arg = e.val[2]
        if f.val == "comma" then
          list.val[k] = self:eval(arg, env)
        end
      end
    end
  end
  print(self:tostring(list))
  return list
end

function llisp:eval(v, env)
  if not env then env = self.global_env end
  if type(v) ~= "table" then return nil, "Expected llisp value, got " ..
                                         type(v) end

  if v.typ == "number" or v.typ == "string" or v.typ == "function" then
     return v
  elseif v.typ == "symbol" then
    -- We should be error handling this
    return self:val_fromlua(self:val_get(v.val, env))
  elseif v.typ == "list" then
    -- Grab the first element, check if it's quote
    local tmp = v.val[1]
    if tmp.typ == "symbol" and tmp.val == "quote" then -- Hate you, quote
      -- return v.val[2]
      return self:handle_comma(v.val[2], env)
    else
      return self:run_function(v)
    end
  else
    print("DEBUG: " .. v.typ)
    error("Please report this error to the maintainter!", 1)
  end
end

function llisp:test() -- placeholder
  local t = {}
  t.tok = self:read()
  self:val_set("a", "Test")
  self:val_set("f", function (v) return v end)
  print(self:pretty_print(self:eval(t.tok)))
end

function llisp:tmp_stdlib()
  self:val_set("t", {typ="symbol", val="t"})
  self:val_set("nil", {typ="symbol", val="nil"})
  self:val_set("exit", function () return os.exit() end)
  self:val_set("+", function (x, y) return x.val + y.val end)
  self:val_set("def", {val=function (sym, exp)
                             return self:val_fromlua(
                                      self:val_set(sym.val, self:eval(exp)))
                           end, typ="macro"})
  self:val_set("eight", {
    val=function (n)
      return {typ="list", val={
                {typ="symbol", val="+"},
                {typ="number", val=8},
                {typ="number", val=n.val}}}
        end, typ="macro"})
end

function llisp:tmp()
  llisp:tmp_stdlib()
  llisp:repl()
  os.exit()
end

-- I'll either impliment a debug_repl, or have it recursively call on repl
-- Something like env, msg, level. So the console looks like `llisp[1]>`
function llisp:repl(env)
  if not env then env = self.global_env end
  local val, msg
  while 1 do
    io.write("llisp> ")
    val, msg = self:read()
    if msg and msg == -1 then
      goto continue
    elseif msg then
      return print("read: " .. msg)
    end
    val, msg = self:expand_macro(val, env)
    if msg then return print("macro-expand: " .. msg) end
    val, msg = self:eval(val, env)
    if msg then return print("eval: " .. msg) end
    val, msg = print("=> " .. self:pretty_print(val))
    ::continue::
  end
end

return llisp
