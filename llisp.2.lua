--[[
-- <<license>>
--]]

llisp = { __version = 0.41 }

llisp.load = loadstring or load
llisp.unpack = table.unpack or unpack
llisp.global_env = {}
llisp.errors = {msg = "",
                input = "",
                output = ""
               }

llisp.flags = {eofp = false,    -- Caught eof?
               spacep = false,-- Is the input just a space char?
               errorp = false
              }

function llisp:split(s, sep)
  -- string -> table. Seperated by sep
  -- Sep must not be a string that lua uses in pattern matching
  local t = {}
  for v in s:gmatch("([^"..sep.."]+)" .. "["..sep.."]*") do
    table.insert(t, v)
  end
  return t
end

function llisp:tostring(v, printkeys, noquotes)
  -- Drop in replacement for tostring()
  -- Except this prints the contents of tables, not the mem addr
  if type(v) == "string" then
    if not noquotes then return "\""..v.."\""
    else return v end
  elseif type(v) == "nil" then return "nil"
  elseif type(v) == "boolean" then
    if v then return "true" else return "false" end
  elseif type(v) == "table" then
    local t = {}
    local tmp = ""
    for k, e in pairs(v) do
      if printkeys then tmp = k.."=" end
      table.insert(t, tmp .. self:tostring(e, printkeys))
    end
    return "{" .. table.concat(t, ", ") .. "}"
  else
    return tostring(v)
  end
end

function llisp:debug_print(s, v)
  -- Since I don't really use this that often I'm wondering
  -- whether or not to keep this.
  if _DEBUG then
    if v then
      io.write(s)
      print(self:tostring(v, true))
    else
      print(self:tostring(s, true))
    end
  end
end

function llisp:table_copy(t)
   return {self.unpack(t)}
end
function llisp:table_concat(t0, t1)
  local t = {self.unpack(t0)}
  for _,e in pairs(t1) do
    table.insert(t, e)
  end
  return t
end

function llisp.stringp(c) return c == "\"" end
function llisp.spacep(c) return c:match("%s") end
function llisp.reservp(c) return c:match("[,'()]+") end
function llisp.commentp(c) return c == ";" end

function llisp:gettoken(str, t, pos)
  local c = str:sub(pos, pos)
  if self.commentp(c) then
    return #str+1 -- Skip to the end of the string
  elseif self.stringp(c) then
    local s = c
    repeat
      pos = pos + 1
      s = s .. str:sub(pos, pos)
    until self.stringp(str:sub(pos, pos))
    pos = pos + 1 -- discard the remaining `"`
    table.insert(t, s)
  elseif self.spacep(c) then
    repeat
      pos = pos + 1
    until not self.spacep(str:sub(pos, pos))
  elseif self.reservp(c) then
    table.insert(t, c)
    pos = pos + 1
  else -- We've got outselves a symbol! Look at the lil' blighter!
    local s = ""
    while pos <= #str and
          (not (self.stringp(c) or
                self.spacep(c)  or
                self.reservp(c) or
                self.commentp(c))) do
      s = s .. c -- It'd be obtuse to say the :sub() stuff here
      pos = pos + 1
      c = str:sub(pos, pos)
    end
    table.insert(t, s)
  end
  return pos
end

function llisp:parse(t)
  -- Construct a parsed table of `t` throwing away input as we go
  -- Llisp values are of the form: {typ=<type>, val=<value>}
  if t == nil then self.flags.eofp = true end
  if next(t) == nil then self.flags.spacep = true return end
  -- ^ If we get a nil input, signal -1 to the repl

  local v = table.remove(t, 1)
  if tonumber(v) then
    return {typ="number", val=tonumber(v)}
  elseif self.stringp(v:sub(1, 1)) then
    return {typ="string", val=v:sub(2, #v-1)}
  elseif v == "(" then
    local nt = {}
    while t[1] ~= ")" do
      table.insert(nt, self:parse(t))
    end
    table.remove(t, 1)
    return {typ="list", val=nt}
  elseif v == "'" then -- We handle quote in eval()
    -- meanwhile we transform it into the form (quote <>)
    return {typ="list", val={
             {typ="symbol", val="quote"},
             self:parse(t)}}
  elseif v == "," then
    -- Same as quote, but this time it's the form (comma <>)
    return {typ="list", val={
             {typ="symbol", val="comma"},
             self:parse(t)}}
  else
    return {typ="symbol", val=v}
  end
end

function llisp.bracketmatchp(t) -- Are the brackets matched?
  local c = 0
  for _, v in pairs(t) do
    if v == "(" then c = c + 1
    elseif v == ")" then c = c - 1 end
  end
  return c == 0
end

function llisp:tokenize(str, h)
  local t = {}
  local pos = 1
  while not (pos > #str) do
    pos = self:gettoken(str, t, pos)
  end
  return t
end

function llisp:read_file(file)
  local h, input, tok
  local tokens = {}
  h = io.open(file)
  if not h then
    self:signal_error("read_file: Could not open file", file)
    return
  end

  while true do
    input = h:read("*l")
    if not input then break end -- hit eof

    tok = self:tokenize(input, h)
    while not self.bracketmatchp(tok) do
      input = h:read("*l")
      if not input then break end
      tok = self:table_concat(tok, self:tokenize(input, h))
    end

    -- Still unmatched brackets? We can't handle that! D:
    if not self.bracketmatchp(tok) then
      self:signal_error("read_file: Brackets unmatched in input")
      return
    end
    -- Make sure the token's not an empty one.
    if next(tok) ~= nil then table.insert(tokens, self:parse(tok)) end
  end
  h:close()
  return tokens
end

function llisp:read_string(str)
  local tok
  if not input then
    self:signal_error("read_string: Empty string or EOF", str)
    return
  end
  tok = self:tokenize(str)
  -- if not tok then
    -- return nil, "???"
  if not self.bracketmatchp(tok) then
    self:signal_error("read_string: Brackets unmatched in input")
    return
  end
  return self:parse(t)
end

function llisp:read()
  local h, input, tok
  h = io.stdin

  input = h:read("*l")
  if not input then
    self:signal_error("read: Empty string or EOF", str)
    return
  end
  tok = self:tokenize(input, h)
  while not self.bracketmatchp(tok) do
    io.write("     > ") -- Gotta have a console! :0
    input = h:read("*l")
    tok = self:table_concat(tok, self:tokenize(input, h))
  end
  if not self.bracketmatchp(tok) then
    self:signal_error("read: Brackets unmatched in input")
    return
  end
  return self:parse(tok)
end

function llisp:val_tollisp(v)
  -- Convert lua values into llisp values
  -- Prototype is: {typ="<typename>", val=<value>}
  if type(v) == "table" and v.typ then
    return v -- return the llisp value
  elseif type(v) == "string" then
    return {typ="string", val=v}
  elseif type(v) == "number" then
    return {typ="number", val=v}
  elseif type(v) == "boolean" or type(v) == "nil" then
    if v then return {typ="symbol", val="t"}
    else      return {typ="symbol", val="nil"} end
  elseif type(v) == "function" then
    return {typ="function", val=v}
  elseif type(v) == "table" then
    local t = {}
    for _, e in pairs(v) do
      table.insert(t, self:val_tollisp(e))
    end
    return {typ="list", val=t}
  end
end

function llisp:val_tolua(v)
  -- {typ="string", val="hello world"} -> "hello world"
  if type(v) == "table" and v.typ then
    if v.typ == "string" or v.typ == "number" or
       v.typ == "function" or v.typ == "macro" then
      return v.val
    elseif v.typ == "symbol" then
      if v.val == "nil" then
        return nil
      elseif v.val == "t" then
        return true
      end
      return v.val
    elseif v.typ == "list" then
      local t = {}
      for _, e in pairs(v.val) do
        table.insert(t, self:val_tolua(e))
      end
      return t
    end
  end
  return v
end

-- Entries in the env look like: {mutp=<bool>,val=<val>}
-- So to avoid the stuff that made my first iteration of this
-- project unreadable (inline jiggery pokery), we use these:

function llisp:val_get(sym, env)
  if not env then env = self.global_env end
  if not env[sym] then return nil end
  return env[sym].val
end

function llisp:val_del(sym, env)
  if not env then env = self.global_env end
  if not env[sym] then
    env[sym] = nil
  end
  return true
end

function llisp:val_set(sym, v, mutp, env)
  if not env then env = self.global_env end
  if mutp ~= nil and mutp ~= false then mutp = true end
  if type(v) ~= "table" or (not v.typ) then
    v = self:val_tollisp(v)
  end
  if env[sym] and env[sym].mutp then return nil end
  env[sym] = {mutp=mutp, val=v}
  return v
end

function llisp:run_function(v, env)
  -- Grab the func, grab the args, execute it
  if v.typ ~= "list" then
    self:signal_error("run_function: Expected list, got ", v.typ)
    return
  end
  local f = self:eval(v.val[1], env)
  local args = self:table_copy(v.val)
  table.remove(args, 1) -- Pop off the function
  for k, e in pairs(args) do
    args[k] = self:eval(e, env)
  end
  table.insert(args, env) -- So the function has context of the environment
  if type(f) == "table" and f.typ ~= "function" then
    if f.typ == "symbol" and f.val == "nil" then
      self:signal_error("First arg of list: Function expected, got ", "nil")
    else
      self:signal_error("First arg of list: Function expected, got ", f.typ)
    end
    return
  end
  -- To avoid any trouble, I'm going to convert the return val to llisp format
  -- print(self:tostring(args, true))
  return self:val_tollisp(f.val(table.unpack(args)))
end

function llisp:run_macro(v, env)
  if v.typ ~= "list" then
    self:signal_error("Expected list, got ", v.typ)
    return
  end
  local m = self:eval(v.val[1], env)
  local args = self:table_copy(v.val)
  table.remove(args, 1)
  if type(m) == "table" and m.typ ~= "macro" then
    self:signal_error("First arg of list: Macro expected, got ", m.typ)
    return
  end
  return self:val_tollisp(m.val(table.unpack(args)))
end

function llisp:expand_macro(v, env)
  if v and v.typ and v.typ == "list" then
    local sym = v.val[1]
    local m = self:val_get(sym.val, env)
    if m and m.typ and m.typ == "macro" then
      return self:run_macro(v, env)
    end
  end
  return v
end

function llisp.commap(v)
  if not v.val[1] then return nil end
  local sym = v.val[1]
  return sym.typ == "symbol" and sym.val == "comma"
end
function llisp:handle_comma(v, env)
  if v.typ ~= "list" then return v end
  -- Case: ,a
  if self.commap(v) then return self:eval(v.val[2], env) end

  -- Case: (,a ((,b c) (,d)) e ,f)
  for k, e in pairs(v.val) do
    if e.typ == "list" and e.val[1] then
      if self.commap(e) then
        v.val[k] = self:eval(e.val[2], env)
      else
        v.val[k] = llisp:handle_comma(e, env)
      end
    end
  end
  return v
end

function llisp.quotep(v)
  local sym = v.val[1]
  return sym.typ == "symbol" and sym.val == "quote"
end
function llisp:handle_quote(v, env)
  -- quote returns the argument verbatim, but we want comma to run as well.
  return self:handle_comma(v, env)
end

function llisp:eval(v, env)
  if not env then env = self.global_env end
  if type(v) ~= "table" then
    llisp:signal_error("eval: Expected llisp value", type(v))
    return
  end
  if v.typ == "number" or v.typ == "string" or v.typ == "function" then
    return v
  elseif v.typ == "symbol" then
    -- Needs error handling!
    return self:val_tollisp(self:val_get(v.val, env))
  elseif v.typ == "list" then
    if self.quotep(v) then
      return self:handle_quote(v.val[2], env)
    elseif self.commap(v) then
      return self:handle_comma(v, env)
    else
      return self:run_function(v, env)
    end
  else
    llisp:signal_error("eval: Expected valid type", llisp:tostring(v))
  end
end

function llisp:eval_batch(t, env)
  -- For use with self:read_file()
  for k, v in pairs(t) do
    t[k] = self:eval(v, env)
  end
end

function llisp:pretty_print(v)
  if type(v) ~= "table" then
    self:signal_error("pretty_print: Expected llisp val", v)
    return
  end

  if v.typ == "string" then
    return "\"" .. v.val .. "\""
  elseif v.typ == "number" then
    return tostring(v.val)
  elseif v.typ == "symbol" then
    return v.val
  elseif v.typ == "function" then
    return "<f:" .. tostring(v.val):sub(11) .. ">"
  elseif v.typ == "macro" then
    return "<m:" .. tostring(v.val):sub(11) .. ">"
  elseif v.typ == "list" then
    local t = {}
    for _, e in pairs(v.val) do
      table.insert(t, self:pretty_print(e))
    end
    return "(" .. table.concat(t, " ") .. ")"
  else
    self:signal_error("pretty_print: Expected valid type", v.typ)
  end
end

function llisp:batch_test(t, env)
  -- t is a table of strings to pass through eval
  local ret = {}
  local i = 1
  for _, v in pairs(t) do
    print("Running test " .. i .. " of " .. #t)
    print("Input  : " .. v)
    ret = llisp:eval(llisp:read_string(v), env)
    print("Output : " ..
      llisp:pretty_print(ret))
    print()
  end
  return ret
end

function llisp:repl(env)
  llisp:load_core() -- This will probably cause problems later on...
  if not env then env = self.global_env end
  local val, msg, pprint
  while true do
    io.write("llisp> ")
    val = self:read()
    if self.flags.eofp then self.flags.eofp = false return end
    if self.flags.spacep then self.flags.spacep = false goto continue end
    if self.flags.errorp then self:debug_repl(0, env) goto continue end

    val = self:expand_macro(val, env)
    if self.flags.errorp then self:debug_repl(0, env) goto continue end

    val = self:eval(val, env)
    if self.flags.errorp then self:debug_repl(0, env) goto continue end

    pprint = self:pretty_print(val)
    if self.flags.errorp then self:debug_repl(0, env) goto continue end
    print("=> " .. pprint)
    ::continue::
  end
end

function llisp:signal_error(msg, cat, input, output)
  self.flags.errorp = true
  if cat then msg = msg .. cat end
  self.errors.msg = msg
  if input then self.errors.input = input end
  if output then self.errors.output = output end
end

function llisp:debug_info()
  print("")
  print("Debug Info:")
  print(self.errors.msg)
  if #self.errors.input > 0 then print("Input:  " .. self.errors.input) end
  if #self.errors.output > 0 then print("Output: " .. self.errors.output) end
  print("-")
  print("To return back to the repl, evaluate (debug:abort)")
  print("To inspect a variable, evaluate (debug:inspect <sym> [<env>])")
end

function llisp:debug_repl(level, env) -- A very kludgey implementation follows:
  if not env then env = self.global_env end
  if not self.flags.errorp then return true end

  local val, pprint
  self.flags.errorp = false
  self.errors.debugp = true

  llisp:debug_info()
  while self.errors.debugp do
    io.write("db[" .. level .. "]> ")

    val = self:read()
    if self.flags.eofp then self.flags.eofp = false return end
    if self.flags.spacep then self.flags.spacep = false goto continue end
    if self.flags.errorp then self:debug_repl(level+1, env) goto continue end

    val = self:expand_macro(val, env)
    if self.flags.errorp then self:debug_repl(level+1, env) goto continue end

    val = self:eval(val, env)
    if self.flags.errorp then self:debug_repl(level+1, env) goto continue end

    pprint = self:pretty_print(val)
    if self.flags.errorp then self:debug_repl(level+1, env) goto continue end
    print(pprint)

    ::continue::
  end
end

function llisp:add_macro(sym, func, env)
  if not env then env = self.global_env end
  self:val_set(sym, {typ="macro", val=func}, nil, env)
end

function llisp:load_core()
  self:val_set("debug:abort",
  function (...)
    self.errors.debugp = false
    return true
  end)

  self:add_macro("debug:inspect",
  function (sym, env)
    return {typ="string",
            val=self:tostring(self:val_get(sym.val))}
  end)

  self:val_set("exit",
  function (code, env)
    if env then code = self:val_tolua(code)
    else code = nil end
    os.exit(code)
    return nil
  end)

  -- self:add_macro("quote",
  -- function (v, env)
  --   return self:handle_quote(v, env)
  -- end)

  self:add_macro("def",
  function (sym, expr, ismut, env)
    if type(ismut) == "table" and (not ismut.typ) then
      env = ismut
      ismut = nil
    end
    -- Yes this *is* disgusting! I'm glad you noticed!
    -- Basically, we have to make this a macro so the symbol doesn't get eval'd.
    -- HOWEVER, because it's a macro, eval gets run on it AFTER that,
    -- so we must quote the result. It's not pretty, but it /works/.
    ret = self:val_set(sym.val, self:eval(expr, env), ismut, env)
    if ret then
      return {val={{val="quote", typ="symbol"}, ret}, typ="list"}
    else
      return self:val_tollisp()
    end
  end)

  self:add_macro("def-glob",
  function (sym, expr, ismut, env)
    if type(ismut) == "table" and (not ismut.typ) then
      env = ismut
      ismut = nil
    end

    ret = self:val_set(sym.val, self:eval(expr, env), ismut)
    if ret then
      return {val={{val="quote", typ="symbol"}, ret}, typ="list"}
    else
      return self:val_tollisp()
    end
  end)

  self:val_set("lua",
  function (v, env)
    if v.typ == "string" then
      local v, msg = self.load("return " .. self:val_tolua(v))
      if not v then return msg
      else
        v = v()
        if type(v) == "function" then
          return function (...)
                    arg = {...}
                    for k,e in pairs(arg) do
                      arg[k] = self:val_tolua(e)
                    end
                    return v(self.unpack(arg))
                  end
        else
          return v
        end
      end
    end
  end)

  self:add_macro("def-lua",
  function (sym, v, env)
    v = self:eval(v, env)
    v = self:val_tolua(v)
    _ENV[sym.val] = v
    if _ENV[sym.val] == v then
      return v
    else return nil end
  end)

end
return llisp
